#!/usr/bin/env python3

import traceback

"""
For your homework this week, you'll be creating a wsgi application of
your own.

You'll create an online calculator that can perform several operations.

You'll need to support:

  * Addition
  * Subtractions
  * Multiplication
  * Division

Your users should be able to send appropriate requests and get back
proper responses. For example, if I open a browser to your wsgi
application at `http://localhost:8080/multiply/3/5' then the response
body in my browser should be `15`.

Consider the following URL/Response body pairs as tests:

```
  http://localhost:8080/multiply/3/5   => 15
  http://localhost:8080/add/23/42      => 65
  http://localhost:8080/subtract/23/42 => -19
  http://localhost:8080/divide/22/11   => 2
  http://localhost:8080/               => <html>Here's how to use this page...</html>
```

To submit your homework:

  * Fork this repository (Session03).
  * Edit this file to meet the homework requirements.
  * Your script should be runnable using `$ python calculator.py`
  * When the script is running, I should be able to view your
    application in my browser.
  * I should also be able to see a home page (http://localhost:8080/)
    that explains how to perform calculations.
  * Commit and push your changes to your fork.
  * Submit a link to your Session03 fork repository!


"""


def add(*args):
    """ Returns a STRING with the sum of the arguments """
    result = int(0)
    if args is None:
      return "0"

    for item in args:
      result += int(item)

    return str(result)

def multiply(*args):
    """ Returns a STRING with the sum of the arguments """
    result = int(1)
    if args is None:
      return "0"

    for item in args:
      result *= int(item)

    return str(result)

def subtract(*args):
    """ Returns a STRING with the sum of the arguments """
    result = int(0)
    if args is None:
      return "0"

    result = int(args[0]) - int(args[1])
    return str(result)

def divide(*args):
    """ Returns a STRING with the sum of the arguments """
    result = int(0)
    if args is None:
      return "0"

    result = int(args[0])/int(args[1])
    return str(result)

def usage(*args):
    usage = \
    """<html> <body>Here's how to use this tool...<br>
    Use the following operators, followed by the operands separated
    by forward slashes
    <ul>\r\n
      <li>add</li>\r\n
      <li>subtract</li>\r\n
      <li>multiply</li>\r\n
      <li>divide</li>
    </ul>
    <br>

    For example, open a browser to the wsgi
    application at `http://localhost:8080/multiply/3/5' then the response
    body in the browser should be `15`. Here are more examples:<br>

    <ul>

      <li>http://localhost:8080/multiply/3/5   => 15</li>
      <li>http://localhost:8080/add/23/42      => 65</li>
      <li>http://localhost:8080/subtract/23/42 => -19</li>
      <li>http://localhost:8080/divide/22/11   => 2</li>
      <li>http://localhost:8080/               => Usage page</li>
    </ul>
    </body></html>
    """
    return usage

def resolve_path(path):
    """
    Should return two values: a callable and an iterable of
    arguments.
    """

    calls = {
        'add' : add,
        'subtract' : subtract,
        'multiply' : multiply,
        'divide' : divide,
        'usage' : usage,
        '' : usage,
    }

    path = path.split('/')
    # path = path.strip('/').split('/')
    try:
      if (len(path) == 2):
          func = calls[path[1]]
          fargs = None
      elif (len(path) >= 3):
          func = calls[path[1]]
          fargs = path[2:]
      else:
          raise NameError
    except KeyError:
      raise NameError

    # TODO: Provide correct values for func and args. The
    # examples provide the correct *syntax*, but you should
    # determine the actual values of func and args using the
    # path.
    # func = add
    # args = ['25', '32']

    return func, fargs

def application(environ, start_response):
    # TODO: Your application code from the book database
    # work here as well! Remember that your application must
    # invoke start_response(status, headers) and also return
    # the body of the response in BYTE encoding.
    #
    # TODO (bonus): Add error handling for a user attempting
    # to divide by zero.
    headers = [('Content-type', 'text/html')]
    try:
        path = environ.get('PATH_INFO', None)
        if path is None:
            raise NameError
        func, args = resolve_path(path)
        if args is not None:
            body = func(*args)
        else:
            body = func()
        status = "200 OK"
    except NameError:
        status = "404 Not Found"
        body = "<h1>Not Found</h1>"
    except Exception:
        status = "500 Internal Server Error"
        body = "<h1>Internal Server Error</h1>"
        print(traceback.format_exc())
    finally:
        headers.append(('Content-length', str(len(body))))
        start_response(status, headers)

    # NOTE: if using python3, you _must_ return a list here
    return [body.encode('utf8')]


if __name__ == '__main__':
    from wsgiref.simple_server import make_server
    srv = make_server('localhost', 8080, application)
    srv.serve_forever()
